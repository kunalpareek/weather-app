import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  authenticate = (username: string, password: string) => {
    let userCredentials: object = {
      username: username,
      password: password
    }
    return this.http.post(`${environment.apiBaseUrl}/auth/login`, userCredentials);
  }
}
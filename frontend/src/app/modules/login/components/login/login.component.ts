import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './../../services/login.service';
import { SnackbarService } from './../../../../services/snackbar.service';
import { LoadingScreenService } from './../../../../services/loading-screen.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private _loginService: LoginService,
    private _router: Router,
    private _snackbar: SnackbarService,
    private _loader: LoadingScreenService
    ){ }

  hide = true;
  username = '';
  password = '';

  ngOnInit(): void {
  }

  login() {
    this._loader.startLoading();
    this._loginService.authenticate(this.username, this.password).subscribe(res => {
      localStorage.setItem('token', res['token'])
      localStorage.setItem('expiry', res['expiry'])
      this._router.navigate(['home']);
      this._loader.stopLoading();
    }, err => {
      this._loader.stopLoading();
      this._snackbar.showSnackbar('Invalid Email or Password!', {type: 'error'});
    })
  }

}

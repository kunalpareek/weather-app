import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  getWeather(cities = []) {
    return this.http.get<any[]>(`${environment.apiBaseUrl}/weather?cities=${cities.join("::")}`);
  }
}

import { Component, OnInit } from '@angular/core';
import { SnackbarService } from './../../../../services/snackbar.service';
import { LoadingScreenService } from './../../../../services/loading-screen.service';
import { WeatherService } from '../../services/weather.service';

import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private _snackbar: SnackbarService,
    private _loader: LoadingScreenService,
    private _weatherSvc: WeatherService
  ) { }

  city = "";
  cities = [];
  weatherData = null;
  weatherDataTicker = [];

  timer: any;
  isRunning: boolean = false;

  ngOnInit(): void {
  }

  search() {
    console.log(this.city);
    this._loader.startLoading();
    this._weatherSvc.getWeather([this.city]).toPromise()
      .then(res => {
        this.weatherData = res[0];
        this._loader.stopLoading();
      })
      .catch(err => {
        this._loader.stopLoading();
        this._snackbar.showSnackbar(err.error.message || 'Error fetching weather data', {type: 'error'});
      })
  }

  searchTicker() {
    this._loader.startLoading();
    this._weatherSvc.getWeather(this.cities).toPromise()
      .then(res => {
        this.weatherDataTicker = res;
        this.startTimer();
        this._loader.stopLoading();
      })
      .catch(err => {
        this._loader.stopLoading();
        this._snackbar.showSnackbar(err.error.message || 'Error fetching weather data', {type: 'error'});
      })
  }

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.cities.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(city): void {
    const index = this.cities.indexOf(city);

    if (index >= 0) {
      this.cities.splice(index, 1);
    }
  }

  startTimer() {
    if (!this.isRunning) {
      this.timer = setInterval(() => {
        this.searchTicker();
      }, 30000); // 30 seconds
      this.isRunning = true;
    }
  }

  stopTimer() {
    if (this.isRunning) {
      clearInterval(this.timer);
      this.isRunning = false;
    }
  }

  timerFunction() {
    console.log('Timer function called');
    // Add your function logic here
  }

  ngOnDestroy() {
    this.stopTimer();
  }

}

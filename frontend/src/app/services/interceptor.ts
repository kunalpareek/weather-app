import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpInterceptor,
    HttpErrorResponse
} from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoadingScreenService } from './loading-screen.service';
import { SnackbarService } from './snackbar.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(
        private router: Router,
        private _loader: LoadingScreenService,
        private _snackbar: SnackbarService
    ) { }
    intercept(request: HttpRequest<any>, next: HttpHandler) {

        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        });
        return next.handle(request).pipe(tap(() => { },
            (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status !== 401) {
                        return;
                    }
                    this._loader.stopLoading();
                    this.router.navigate(['login']);
                    this._snackbar.showSnackbar('Session timed out. Login again to continue.', { type: 'error' })
                }
            }));
    }
}
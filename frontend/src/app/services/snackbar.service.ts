import { Injectable } from '@angular/core';
import {MatSnackBar } from '@angular/material/snack-bar';
import {SnackbarComponent} from './../components/snackbar/snackbar.component';

import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';

export interface Snackbar{ 
    duration?: number,
    type?: string,
    verticalPosition?: MatSnackBarVerticalPosition,
    horizontalPosition?: MatSnackBarHorizontalPosition,
    action?: string
}


@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private _snackBar: MatSnackBar) { }

  showSnackbar( message: string, { duration = 5000, type = 'info',verticalPosition = 'top', horizontalPosition = 'right', action = '' } : Snackbar = {}){
    if(message){
      this._snackBar.openFromComponent(SnackbarComponent, {
        duration: duration,
        panelClass: ['snackbar',type],
        verticalPosition: verticalPosition,
        horizontalPosition: horizontalPosition,
        data: { message: message, type: type }
      });
    }
  }
}

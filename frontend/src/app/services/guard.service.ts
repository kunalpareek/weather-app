import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(private _router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem("token") && (new Date().getTime() / 1000) < parseInt(localStorage.getItem("expiry"))) {
      if (state.url === "/login"){
        this._router.navigate(['/dashboard']);    
      }  
      return true;
    }
    this._router.navigate(['/login']);
    return false;
}

}
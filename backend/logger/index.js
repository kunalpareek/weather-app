"use strict";

const winston = require("winston");
let logger;

class Logger {
    static async init() {
        if (logger) {
            return
        }
        try {
            let options = {
                console: {
                    level: "debug",
                    handleExceptions: true,
                    json: false,
                    colorize: true,
                    timestamp: true
                }
            };
            logger = winston.createLogger({
                format: winston.format.combine(
                    winston.format.timestamp(),
                    winston.format.json()
                ),
                transports: [
                    new winston.transports.Console(options.console)
                ],
                exitOnError: false
            });
            
            logger.stream = {
                write: function (msg) {
                    logger.info(msg);
                }
            };
        } catch (err) {
            console.log("error initializing logger", err);
        }
    }

    static async log(level, message, data) {
        logger.log(level, message, { logDetails: data });
    }
}

module.exports = Logger;
"use strict";

const app = require("./app");
const http = require("http");
const config = require("./config");
const logger = require("./logger");

const server = http.createServer(app);

server.listen(config.port, (err) => {
    if (err) {
        logger.log("error", "Server error", { "message": err });
    } else {
        logger.log("info", `server running at  ${config.port}`, null);
    }
});
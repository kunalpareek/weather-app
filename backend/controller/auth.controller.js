'use strict';

const authSvc = require('./../service/auth.service');

const authenticateUser = (req, res, next) => {
    authSvc.authenticateUser(req.body)
        .then(result => {
            res.status(200).json(result)
        })
        .catch(err => {
            next(err);
        })
};

module.exports = {
    authenticateUser
}
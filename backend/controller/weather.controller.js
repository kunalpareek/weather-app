'use strict';

const weatherSvc = require('./../service/weather.service');

const getWeather = (req, res, next) => {
    weatherSvc.getWeather(req.query)
        .then(result => {
            res.status(200).json(result)
        })
        .catch(err => {
            next(err);
        })
};

module.exports = {
    getWeather
}
module.exports = {
    "port": process.env.PORT || 3000,
    "username": process.env.USER_NAME,
    "password": process.env.PASSWORD,
    "jwtSecret": process.env.JWT_SECRET,
    "geocodeApiKey": process.env.GEOCODE_API_KEY
};
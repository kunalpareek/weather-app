"use strict";

const router = require("express").Router();

const authMiddleware = require('./../middleware/auth');
const requestValidator = require('./../middleware/request-validation');
const requestSchema = require('./../controller/schema');
const authController = require('./../controller/auth.controller');
const weatherController = require('./../controller/weather.controller');

router.post('/auth/login', requestValidator.validateRequestBody(requestSchema.login), authController.authenticateUser);
router.get('/weather', authMiddleware, requestValidator.validateQueryParams(requestSchema.getWeather), weatherController.getWeather);

module.exports = router;
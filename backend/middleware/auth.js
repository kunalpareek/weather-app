"use strict";

const jwt = require("jsonwebtoken");
const config = require("./../config");

const authMiddleware = (req, res, next) => {
    try {
        if (req.headers["authorization"]) {
            let token = req.headers["authorization"].replace("Bearer ", "");
            let decoded = jwt.verify(token, config.jwtSecret)
            req.headers["x-user"] = decoded.username;
            next()
        } else {
            throw new Error("Authorization header missing")
        }
    } catch (err) {
        res.status(401).json({"message": "Unauthorized"})
    }
};

module.exports = authMiddleware;
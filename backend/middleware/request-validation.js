"use strict";

const Ajv = require("ajv");
const ajv = new Ajv({ allErrors: true });

const validateRequestBody = (schema) => {
    return (req, res, next) => {
        try {
            const validate = ajv.compile(schema);
            if (!validate(req.body)) {
                let err = new Error("Error in request body, please check details to find all errors.");
                err.status = 400;
                err.details = validate.errors;
                throw err;
            } else {
                next();
            }
        } catch (err) {
            const status = err.status || 500;
            res.status(status).json({
                "error": {
                    "code": err.code || undefined,
                    "details": err.details || undefined,
                    "message": err.message || "Internal server error"
                }
            });
        }
    };
};

const validateQueryParams = (schema) => {
    return (req, res, next) => {
        try {
            const validate = ajv.compile(schema);
            if (!validate(req.query)) {
                let err = new Error("Error in query params, please check details to find all errors.");
                err.status = 400;
                err.details = validate.errors;
                throw err;
            } else {
                next();
            }
        } catch (err) {
            const status = err.status || 500;
            res.status(status).json({
                "error": {
                    "code": err.code || undefined,
                    "details": err.details || undefined,
                    "message": err.message || "Internal server error"
                },
                "status": status,
                "message": err.message || "Internal server error"
            });
        }
    };
};

module.exports = {
    validateRequestBody,
    validateQueryParams
};
"use strict";

const logger = require('./../logger');

const errorHandler = (err, req, res, next) => {
    let errObj = {};
    let status = err.status || 500;

    if (res.headersSent) {
        return next(err);
    }

    errObj["status"] = status;
    errObj["details"] = err.details || undefined;
    errObj["message"] = err.message || "Internal server error.";
    logger.log('error', errObj.message, errObj);
    res.status(status).json(errObj);
};

module.exports = errorHandler;
'use strict';

const axios = require("axios");
const config = require("./../config");
const logger = require("./../logger");

const getWeather = async (queryParams) => {
    const cities = queryParams.cities.split("::")
    if (cities.length > 3) {
        const err = new Error("Maximum 3 cities supported in one request");
        err.status = 400;
        throw err;
    }
    const response = [];
    for (let i = 0; i < cities.length; i++) {
        const coordinates = await getLatLong(cities[i]);
        coordinates.query = cities[i];
        const weather = await getWeatherData(coordinates.lat, coordinates.long);
        response.push({ coordinates, weather });
    }
    return response;
}

const getLatLong = async (address) => {
    try {
        const res = await axios({
            method: "GET",
            url: `https://geocode.maps.co/search?q=${address}&api_key=${config.geocodeApiKey}`,
        });
        const coordinates = res.data.length && res.data[0];
        return { lat: coordinates.lat, long: coordinates.lon, displayName: coordinates.display_name };
    } catch (err) {
        logger.log(err);
        const error = new Error("Error fetching coordinates")
        throw error;
    }
}

const getWeatherData = async (lat, long) => {
    try {
        console.log(`https://api.open-meteo.com/v1/forecast?latitude=${lat}&longitude=${long}&current=temperature_2m,is_day,precipitation,rain&hourly=temperature_2m,precipitation_probability,precipitation`)
        const res = await axios({
            method: "GET",
            url: `https://api.open-meteo.com/v1/forecast?latitude=${lat}&longitude=${long}&current=temperature_2m,is_day,precipitation,rain&hourly=temperature_2m,precipitation_probability,precipitation`
        })
        return res.data;
    } catch (err) {
        logger.log(err);
        const error = new Error("Error fetching weather")
        throw error;
    }
}

module.exports = {
    getWeather
}
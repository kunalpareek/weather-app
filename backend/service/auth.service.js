'use strict';

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require('./../config');

const authenticateUser = async (data) => {
    const usernameMatch = data.username === config.username;
    const passwordMatch = await compareBcrypt(data.password, config.password);
    if (usernameMatch && passwordMatch) {
        return {
            "token": jwt.sign({ username: data.username }, config.jwtSecret, { expiresIn: "1h" }),
            "expiry": new Date().getTime() + (60 * 60 * 1000),
            "username": data.username
        };
    }
    const err = new Error("Invalid username or password.")
    err.status = 400;
    throw err;
};

const compareBcrypt = async (input, hash) => {
    const doTheyMatch = await new Promise((resolve, reject) => {
      bcrypt.compare(input, hash, function (err, match) {
        if (err) reject(err)
        resolve(match)
      });
    })

    return doTheyMatch;
};

module.exports = {
    authenticateUser
}
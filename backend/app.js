"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const morganLogger = require("morgan");
const errorHandler = require("./middleware/error-handler");
const helmet = require("helmet");
const cors = require("cors");

const logger = require("./logger");
logger.init()

const routes = require("./routes");
let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(helmet());
app.use(cors());

let router = express.Router();

router.get("/ping", (req, res) => {
    res.status(200).json({ "message": "ok" });
});

app.use(morganLogger('dev'));

app.use("/", router);
app.use("/api/v1/", routes);

app.use(express.static("public"));

app.use(errorHandler);

module.exports = app;